Ventajas:

Mantenibilidad
testeabilidad
Flexibilidad y extensibilidad
Desarrollo paralelo
Loose Coupling


Cada clase y m�dulo debe enfocarse en una sola tarea al tiempo.
Cada cosa en la clase debe estar destinada a un solo prop�sito.
Pueden haber muchos miembros en la clase si bien se relacionan con la �nica responsabilidad.
Con el SRP (single responsability principle) las clases pueden ser m�s peque�as y limpias.
Code is less fragile. 


Identificar responsabilidad permitiendos�nos construir mejor secciones de c�digo.

Si nuesra clase tiene muchos if o or quiz� estemos vilando el principio SRP.

Una clase solo cambia por una �nica funci�n (antipatr�n clase Dios, c�digo espagueti).
Esta asociado con el t�rmino de cohesi�n (correlaci�n de tem�tica con los elementos de un m�dulo).

SoC: filosof�a de divide y vencr�s.

Debemos:
Separa responsabilidades y una interfaz que nos permita desacoplar las implementaciones del main.

La l�gica de ejecuci�n mezclada con la de inicializaci�n (inicializaci�n vaga). Es decir, separa la l�gica de ejecuci�n 
de otros procesos.

Interfaces con responsabilidades correlacionadas.

Buscar el bajo acoplamiento y maximizar la cohesi�n.

Factorizar el c�digo para minimizar la complejidad.

La idea es que las clases sean los suficientemente peque�as para minimizar el acoplamiento y lo suficientemente 
grande para maximizar la coheci�n.  

No se debe confundir de que cada clase tenga un m�todo. Una clase puede tener varios m�todos. Pero, todos enfocados
a una sola responsabilidad.

Si una clase tiene m�s de un m�todo va a ser m�s dificil mantenerlo, corregir, expandir, etc.

La clase solo debe tener una sola raz�n para cambiar.

El c�digo va a ser menos complejo, mas facil de mantener y reutilizar, hay m�s coherencia entre los m�todos. La clase
y los m�todos tienden a ser m�s compactos.

Es algo que se va adquiriendo con la experiencia. En proyectos previamente creado es muy dificil aplicarlo (desventaja).

https://www.youtube.com/watch?v=QnC2dmrzfXk
https://www.youtube.com/watch?v=I_NgCT2EI3E

CLAVE: La raz�n por la cual puede cambiar la clase solo debe estar relacionada a su responsabilidad. Identficicar
lo que no pertenece a la responsabilidad de la clase y sacarlo.




