using System;
using System.Text;

namespace not_apply.Employee
{
    public class NewEmployee
    {
        private string name;
        private string job;
        private int age;
        private double salary;

        public NewEmployee(string Name, string Job, int Age, double Salary)
        {
            this.name = Name;
            this.job = Job;
            this.age = Age;
            this.salary = Salary;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", name, job, age, salary);
        }

        public double TotalTaxes()
        {
            return salary * 0.05;
        }

        public void TaxesToPay()
        {
            double taxes = TotalTaxes() + (salary/1000) * 4;
            Console.WriteLine("{0} has paid {1} colombian pesos taxes.", name, taxes);
        }
    }
}