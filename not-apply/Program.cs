﻿using System;
using not_apply.Employee;

namespace not_apply
{
    class Program
    {
        static void Main(string[] args)
        {
            NewEmployee newEmploy = new NewEmployee("Martin", "Software Developer", 25, 4530000);
            Console.WriteLine(newEmploy);
            newEmploy.TaxesToPay();                    
        }
    }
}
