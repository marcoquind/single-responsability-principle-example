﻿using System;
using applied_SRP.Employee;
using applied_SRP.Employee.Taxes;

namespace applied_SRP
{
    class Program
    {
        static void Main(string[] args)
        {
            string Name = "Martin";
            string Job = "Software Developer";
            int Age = 25;
            double Salary = 4530000;
            
            INewEmployee newEmployee = new NewEmployee(Name, Job, Age, Salary);
            Console.WriteLine(newEmployee.PrintNewEmployee());
            ITaxesCalculator taxesCalculator = new TaxesCalculator(Name, Salary);
            Console.WriteLine("\nTotal taxes to pay: " + taxesCalculator.TaxesToPay());
        }
    }
}
