using System;
using System.Text;

namespace applied_SRP.Employee
{
    class NewEmployee : INewEmployee
    {
        private string name;
        private string job;
        private int age;
        private double salary;

        public NewEmployee(string Name, string Job, int Age, double Salary)
        {
            this.name = Name;
            this.job = Job;
            this.age = Age;
            this.salary = Salary;
        }


        public string PrintNewEmployee()
        {
            return "Employee: " + name + 
                    "\nJob: " + job +
                    "\nAge: " + age +
                    "\nSalary: " + salary;
        }
    }
}