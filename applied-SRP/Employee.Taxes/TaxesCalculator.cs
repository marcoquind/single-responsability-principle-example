using System;

namespace applied_SRP.Employee.Taxes
{
    public class TaxesCalculator : ITaxesCalculator
    {
        private string name;
        private double salary;
        public double taxesToPay;

        public TaxesCalculator(string Name, double Salary)
        {
            this.name = Name;
            this.salary = Salary;
        }
        
        public double taxes()
        {
            return salary * 0.05;
        }

        public double TaxesToPay()
        {
            return this.taxesToPay = taxes() + (salary/1000) * 4;
        }
    }
}