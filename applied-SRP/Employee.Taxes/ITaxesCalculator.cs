namespace applied_SRP.Employee.Taxes
{
    public interface ITaxesCalculator
    {
         double taxes();
         double TaxesToPay();
    }
}